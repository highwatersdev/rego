# rego

Key-vault in-memory store written in Go

## Installation

1. Clone the repository with git command

```console
git clone https://gitlab.com/highwatersdev/rego.git
```

and switch to the directory `cd rego`

2. Build container image using docker command

```console
docker build .
```

3. Push the image to your container registry as required

4. Deploy to Kubernetes using kubectl command

```console
kubectl apply -f rego-app.yaml
```

**Note:** replace image in Kubernetes YAML definition with your image reference

5. Update ingress host in `rego-app.yaml` with your FQDN

6. Access the app `https://<your_fqdn>:9000`

## Usage

- `/get/{key}` to get the value of the `{key}`. HTTP GET method

- `/set` to add new key/value pair, send HTTP POST request with body containing the key(s) {"key1":value1}

- `search` to search in the store for the key

  - `/search?prefix={key}` to search based on prefix

  - `/search?suffix={1}` to search based on suffix

## Metrics

To access application metrics:

```console
kubectl port-forward <grafana-pod> 3000:3000
```

and navigate to http://localhost:3000
