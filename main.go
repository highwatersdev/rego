package main

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"log"
	"net/http"
	"strconv"
	"strings"
)

//Prometheus metrics//
type responseWriter struct {
	http.ResponseWriter
	statusCode int
}

func NewResponseWriter(w http.ResponseWriter) *responseWriter {
	return &responseWriter{w, http.StatusOK}
}

func (rw *responseWriter) WriteHeader(code int) {
	rw.statusCode = code
	rw.ResponseWriter.WriteHeader(code)
}

var totalRequests = prometheus.NewCounterVec(
	prometheus.CounterOpts{
		Name: "http_requests_total",
		Help: "Number of get requests.",
	},
	[]string{"path"},
)

var responseStatus = prometheus.NewCounterVec(
	prometheus.CounterOpts{
		Name: "response_status",
		Help: "Status of HTTP response",
	},
	[]string{"status"},
)

//var numberOfKeys = prometheus.NewCounterVec(
//	prometheus.CounterOpts{
//		Namespace: "golang",
//		Name:      "number_of_keys",
//		Help:      "Number of keys in the kv store",
//	},
//	[]string{"count"})

var httpDuration = promauto.NewHistogramVec(prometheus.HistogramOpts{
	Name: "http_response_time_seconds",
	Help: "Duration of HTTP requests.",
}, []string{"path"})

func prometheusMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		route := mux.CurrentRoute(r)
		path, _ := route.GetPathTemplate()

		timer := prometheus.NewTimer(httpDuration.WithLabelValues(path))
		rw := NewResponseWriter(w)
		next.ServeHTTP(rw, r)

		statusCode := rw.statusCode

		responseStatus.WithLabelValues(strconv.Itoa(statusCode)).Inc()
		totalRequests.WithLabelValues(path).Inc()

		//kvStoreLength := len(kvStore)
		//numberOfKeys.WithLabelValues(strconv.Itoa(kvStoreLength)).Inc()

		timer.ObserveDuration()
	})
}

func init() {
	prometheus.Register(totalRequests)
	prometheus.Register(responseStatus)
	prometheus.Register(httpDuration)
	//	prometheus.Register(numberOfKeys)
}

//End Prometheus metrics//

//HTTP API methods//
var kvStore = make(map[string]interface{})

var Get = func(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	vars := mux.Vars(r)
	_, ok := kvStore[vars["key"]]
	if !ok {
		http.Error(w, "Key not found", http.StatusBadRequest)
		return
	}
	_ = json.NewEncoder(w).Encode(kvStore[vars["key"]])

}

var Set = func(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var newVal map[string]interface{}
	err := json.NewDecoder(r.Body).Decode(&newVal)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		panic(err)
	}

	for k, v := range newVal {
		kvStore[k] = v
	}
	_ = json.NewEncoder(w).Encode(kvStore)
}

var Search = func(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var arr []string
	err := r.ParseForm()
	if err != nil {
		panic(err)
	}
	queries := r.Form

	if len(queries["prefix"]) > 0 {
		for k := range kvStore {
			if strings.HasPrefix(k, queries["prefix"][0]) {
				arr = append(arr, k)
			}
		}
	}

	if len(queries["suffix"]) > 0 {
		for k := range kvStore {
			if strings.HasSuffix(k, queries["suffix"][0]) {
				arr = append(arr, k)
			}
		}
	}

	_ = json.NewEncoder(w).Encode(arr)
}

//End HTTP API methods//

func main() {
	router := mux.NewRouter()
	router.Use(prometheusMiddleware)

	router.Path("/prometheus").Handler(promhttp.Handler())

	router.HandleFunc("/get/{key}", Get).Methods("GET")
	router.HandleFunc("/set", Set).Methods("POST")
	router.HandleFunc("/search", Search).Queries("prefix", "{str}").Methods("GET")
	router.HandleFunc("/search", Search).Queries("suffix", "{str}").Methods("GET")

	log.Println("Listening on port 9000")
	log.Fatal(http.ListenAndServe(":9000", router))
}
